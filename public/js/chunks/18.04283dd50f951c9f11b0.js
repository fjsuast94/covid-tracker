(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[18],{

/***/ "./frontend/src/views/hospital/HospitalView.vue":
/*!******************************************************!*\
  !*** ./frontend/src/views/hospital/HospitalView.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HospitalView_vue_vue_type_template_id_7fc1614e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HospitalView.vue?vue&type=template&id=7fc1614e& */ "./frontend/src/views/hospital/HospitalView.vue?vue&type=template&id=7fc1614e&");
/* harmony import */ var _HospitalView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HospitalView.vue?vue&type=script&lang=js& */ "./frontend/src/views/hospital/HospitalView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _HospitalView_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./HospitalView.vue?vue&type=style&index=0&lang=scss& */ "./frontend/src/views/hospital/HospitalView.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _HospitalView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HospitalView_vue_vue_type_template_id_7fc1614e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HospitalView_vue_vue_type_template_id_7fc1614e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/src/views/hospital/HospitalView.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./frontend/src/views/hospital/HospitalView.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./frontend/src/views/hospital/HospitalView.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HospitalView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./HospitalView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/hospital/HospitalView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HospitalView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./frontend/src/views/hospital/HospitalView.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************************!*\
  !*** ./frontend/src/views/hospital/HospitalView.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HospitalView_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/sass-loader/dist/cjs.js??ref--11-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./HospitalView.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/hospital/HospitalView.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HospitalView_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HospitalView_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HospitalView_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HospitalView_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./frontend/src/views/hospital/HospitalView.vue?vue&type=template&id=7fc1614e&":
/*!*************************************************************************************!*\
  !*** ./frontend/src/views/hospital/HospitalView.vue?vue&type=template&id=7fc1614e& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HospitalView_vue_vue_type_template_id_7fc1614e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./HospitalView.vue?vue&type=template&id=7fc1614e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/hospital/HospitalView.vue?vue&type=template&id=7fc1614e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HospitalView_vue_vue_type_template_id_7fc1614e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HospitalView_vue_vue_type_template_id_7fc1614e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/hospital/HospitalView.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/views/hospital/HospitalView.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, exports) {

throw new Error("Module build failed (from ./node_modules/babel-loader/lib/index.js):\nSyntaxError: C:\\xampp\\htdocs\\covid-tracker\\frontend\\src\\views\\hospital\\HospitalView.vue: Unexpected token, expected \",\" (117:4)\n\n\u001b[0m \u001b[90m 115 |\u001b[39m   components\u001b[33m:\u001b[39m {\u001b[0m\n\u001b[0m \u001b[90m 116 |\u001b[39m     \u001b[33mStatisticCardVertical\u001b[39m\u001b[0m\n\u001b[0m\u001b[31m\u001b[1m>\u001b[22m\u001b[39m\u001b[90m 117 |\u001b[39m     \u001b[33mBCardImg\u001b[39m\u001b[33m,\u001b[39m\u001b[0m\n\u001b[0m \u001b[90m     |\u001b[39m     \u001b[31m\u001b[1m^\u001b[22m\u001b[39m\u001b[0m\n\u001b[0m \u001b[90m 118 |\u001b[39m     \u001b[33mBCardBody\u001b[39m\u001b[33m,\u001b[39m\u001b[0m\n\u001b[0m \u001b[90m 119 |\u001b[39m     \u001b[33mBFormInput\u001b[39m\u001b[33m,\u001b[39m\u001b[0m\n\u001b[0m \u001b[90m 120 |\u001b[39m     \u001b[33mBMedia\u001b[39m\u001b[33m,\u001b[39m\u001b[0m\n    at Object._raise (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:810:17)\n    at Object.raiseWithData (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:803:17)\n    at Object.raise (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:764:17)\n    at Object.unexpected (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:9967:16)\n    at Object.expect (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:9941:28)\n    at Object.parseObjectLike (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:11730:14)\n    at Object.parseExprAtom (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:11289:23)\n    at Object.parseExprAtom (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:5237:20)\n    at Object.parseExprSubscripts (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10941:23)\n    at Object.parseUpdate (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10921:21)\n    at Object.parseMaybeUnary (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10899:23)\n    at Object.parseExprOps (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10756:23)\n    at Object.parseMaybeConditional (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10730:23)\n    at Object.parseMaybeAssign (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10693:21)\n    at C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10660:39\n    at Object.allowInAnd (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:12356:12)\n    at Object.parseMaybeAssignAllowIn (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10660:17)\n    at Object.parseObjectProperty (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:11899:101)\n    at Object.parseObjPropValue (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:11924:100)\n    at Object.parsePropertyDefinition (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:11848:10)\n    at Object.parseObjectLike (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:11738:25)\n    at Object.parseExprAtom (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:11289:23)\n    at Object.parseExprAtom (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:5237:20)\n    at Object.parseExprSubscripts (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10941:23)\n    at Object.parseUpdate (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10921:21)\n    at Object.parseMaybeUnary (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10899:23)\n    at Object.parseExprOps (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10756:23)\n    at Object.parseMaybeConditional (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10730:23)\n    at Object.parseMaybeAssign (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10693:21)\n    at C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10660:39\n    at Object.allowInAnd (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:12350:16)\n    at Object.parseMaybeAssignAllowIn (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:10660:17)\n    at Object.parseExportDefaultExpression (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:13763:24)\n    at Object.parseExport (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:13673:31)\n    at Object.parseStatementContent (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:12657:27)\n    at Object.parseStatement (C:\\xampp\\htdocs\\covid-tracker\\node_modules\\@babel\\parser\\lib\\index.js:12551:17)");

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/hospital/HospitalView.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/sass-loader/dist/cjs.js??ref--11-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/views/hospital/HospitalView.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".blog-list-wrapper .blog-title-truncate {\n  display: -webkit-box;\n  -webkit-line-clamp: 2;\n  -webkit-box-orient: vertical;\n  overflow: hidden;\n}\n.blog-list-wrapper .blog-content-truncate {\n  display: -webkit-box;\n  -webkit-line-clamp: 3;\n  -webkit-box-orient: vertical;\n  overflow: hidden;\n}\n.blog-detail-wrapper .blog-detail-share .dropdown-menu {\n  min-width: auto;\n}\n.blog-sidebar .blog-recent-posts img {\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.blog-sidebar .blog-recent-posts .text-body-heading:hover {\n  color: #5e50ee !important;\n}\n.blog-sidebar .blog-recent-post-title,\n.blog-sidebar .blog-category-title {\n  line-height: 23px;\n  letter-spacing: 0;\n}\n[dir] .blog-edit-wrapper .border {\n  border-color: #d8d6de !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/hospital/HospitalView.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/sass-loader/dist/cjs.js??ref--11-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/views/hospital/HospitalView.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/sass-loader/dist/cjs.js??ref--11-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./HospitalView.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/hospital/HospitalView.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/hospital/HospitalView.vue?vue&type=template&id=7fc1614e&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/views/hospital/HospitalView.vue?vue&type=template&id=7fc1614e& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return Object.keys(_vm.hospitalDetail)
    ? _c(
        "content-with-sidebar",
        { staticClass: "cws-container cws-sidebar-right blog-wrapper" },
        [
          _c(
            "b-card",
            { staticClass: "overflow-hidden", attrs: { "no-body": "" } },
            [
              _c(
                "b-row",
                { attrs: { "no-gutters": "" } },
                [
                  _c(
                    "b-col",
                    { attrs: { md: "6" } },
                    [
                      _c("b-card-img", {
                        staticClass: "rounded-0",
                        attrs: {
                          src: _vm.hospitalDetail.image,
                          alt: _vm.hospitalDetail.image
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { attrs: { md: "6" } },
                    [
                      _c(
                        "b-card-body",
                        { attrs: { title: _vm.hospitalDetail.name } },
                        [
                          _c(
                            "b-media",
                            { attrs: { "no-body": "" } },
                            [
                              _c(
                                "b-media-aside",
                                {
                                  staticClass: "mr-50",
                                  attrs: { "vertical-align": "center" }
                                },
                                [
                                  _c("b-avatar", {
                                    attrs: {
                                      href: "javascript:void(0)",
                                      size: "24",
                                      src: _vm.hospitalDetail.image
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("b-media-body", [
                                _c(
                                  "small",
                                  { staticClass: "text-muted mr-50" },
                                  [_vm._v("by")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "small",
                                  [
                                    _c("b-link", { staticClass: "text-body" }, [
                                      _vm._v(_vm._s(_vm.hospitalDetail.name))
                                    ])
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "text-muted ml-75 mr-50" },
                                  [_vm._v("|")]
                                ),
                                _vm._v(" "),
                                _c("small", { staticClass: "text-muted" }, [
                                  _vm._v(_vm._s(_vm.hospitalDetail.ago))
                                ])
                              ])
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "my-1 py-25" },
                            [
                              _c(
                                "b-link",
                                [
                                  _c(
                                    "b-badge",
                                    {
                                      staticClass: "mr-75",
                                      attrs: {
                                        pill: "",
                                        variant: _vm.tagsColor(
                                          _vm.hospitalDetail.slug
                                        )
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                " +
                                          _vm._s(_vm.hospitalDetail.slug) +
                                          "\n              "
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "blog-content",
                            domProps: {
                              innerHTML: _vm._s(_vm.hospitalDetail.address)
                            }
                          }),
                          _vm._v(" "),
                          _c("statistic-card-vertical", {
                            attrs: {
                              icon: "EyeIcon",
                              statistic: "36.9k",
                              "statistic-title": "Views",
                              color: "info"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "blog-detail-wrapper" },
            [
              _c(
                "b-row",
                [
                  _c(
                    "b-col",
                    { attrs: { cols: "12" } },
                    [
                      _c(
                        "b-card",
                        {
                          attrs: {
                            "img-src": _vm.hospitalDetail.image,
                            "img-top": "",
                            "img-height": "300",
                            "img-alt": "Blog Detail Pic",
                            title: _vm.hospitalDetail.name
                          }
                        },
                        [
                          _c(
                            "b-media",
                            { attrs: { "no-body": "" } },
                            [
                              _c(
                                "b-media-aside",
                                {
                                  staticClass: "mr-50",
                                  attrs: { "vertical-align": "center" }
                                },
                                [
                                  _c("b-avatar", {
                                    attrs: {
                                      href: "javascript:void(0)",
                                      size: "24",
                                      src: _vm.hospitalDetail.image
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("b-media-body", [
                                _c(
                                  "small",
                                  { staticClass: "text-muted mr-50" },
                                  [_vm._v("by")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "small",
                                  [
                                    _c("b-link", { staticClass: "text-body" }, [
                                      _vm._v(_vm._s(_vm.hospitalDetail.name))
                                    ])
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "text-muted ml-75 mr-50" },
                                  [_vm._v("|")]
                                ),
                                _vm._v(" "),
                                _c("small", { staticClass: "text-muted" }, [
                                  _vm._v(_vm._s(_vm.hospitalDetail.ago))
                                ])
                              ])
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "my-1 py-25" },
                            [
                              _c(
                                "b-link",
                                [
                                  _c(
                                    "b-badge",
                                    {
                                      staticClass: "mr-75",
                                      attrs: {
                                        pill: "",
                                        variant: _vm.tagsColor(
                                          _vm.hospitalDetail.slug
                                        )
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                " +
                                          _vm._s(_vm.hospitalDetail.slug) +
                                          "\n              "
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "blog-content",
                            domProps: {
                              innerHTML: _vm._s(_vm.hospitalDetail.description)
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);