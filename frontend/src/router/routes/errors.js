export default [{
        path: '/error-404',
        name: 'error-404',
        component: () =>
            import ('@/views/error/Error404.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
            action: 'read',
        }
    },
    {
        path: '/not-authorized',
        name: 'not-authorized',
        component: () =>
            import ('@/views/pages/miscellaneous/NotAuthorized.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
        },
    },
    {
        path: '/coming-soon',
        name: 'coming-soon',
        component: () =>
            import ('@/views/pages/miscellaneous/ComingSoon.vue'),
        meta: {
            layout: 'full',
        },
    },
]