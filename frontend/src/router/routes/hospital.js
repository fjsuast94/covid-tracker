export default [

    {
        path: '/admin/hospital',
        name: 'admin-hospital',
        component: () =>
            import ('@/views/hospital/HospitalList.vue'),
        meta: {
            pageTitle: 'Hospital',
            breadcrumb: [{
                text: 'Lista de hospitales',
                active: true,
            }, ],
        }
    },
    {
        path: '/admin/hospital/add',
        name: 'admin-hospital-add',
        component: () =>
            import ('@/views/hospital/HospitalAdd.vue'),
        meta: {
            pageTitle: 'Hospital',
            breadcrumb: [

                {
                    text: 'Lista de hospitales',
                },
                {
                    text: 'Agregar',
                    active: true,
                },
            ],
        }
    },
    {
        path: '/admin/hospital/view/:id',
        name: 'admin-hospital-view',
        component: () =>
            import ('@/views/hospital/HospitalView.vue'),
        meta: {
            pageTitle: 'Hospital',
            breadcrumb: [{
                    text: 'Lista de hospitales',
                },
                {
                    text: 'Detalle de hospital',
                    active: true,
                },
            ],
        }
    },
]