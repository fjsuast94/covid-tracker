export default [

    {
        path: '/admin/users/list',
        name: 'admin-users-list',
        component: () =>
            import ('@/views/user/users-list/UsersList.vue'),
        meta: {
            resource: 'Auth',
        }
    },
    {
        path: '/admin/users/view/:id',
        name: 'admin-users-view',
        component: () =>
            import ('@/views/user/users-view/UsersView.vue'),
        meta: {
            resource: 'Auth',
        }
    },
    {
        path: '/admin/users/edit/:id',
        name: 'admin-users-edit',
        component: () =>
            import ('@/views/user/users-edit/UsersEdit.vue'),
        meta: {
            resource: 'Auth',
        }
    },
    {
        path: '/admin/bitacora',
        name: 'bitacora',
        component: () =>
            import ('@/views/bitacora/bitacora.vue'),
        meta: {
            resource: 'Auth',
            pageTitle: 'Bitacora',
            breadcrumb: [{
                text: 'Seguimiento',
                active: true,
            }, ],
        },
    },
]