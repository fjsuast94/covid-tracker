import Vue from 'vue'

// axios
import axios from 'axios'
import { $themeConfig } from '@themeConfig'

import router from '@/router'

const axiosIns = axios.create({
    // baseURL: $themeConfig.api,
    // timeout: 1000,
    // You can add your headers here
    // ================================
    // baseURL: 'https://some-domain.com/api/',
    // headers: {'X-Custom-Header': 'foobar'}
})

axiosIns.interceptors.response.use(function(response) {
    return response
}, function(error) {
    return Promise.reject(error)
})

Vue.prototype.$http = axiosIns

export default axiosIns