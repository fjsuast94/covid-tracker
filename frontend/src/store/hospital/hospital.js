export default {
    namespaced: true,
    state: {
        data: {}
    },
    mutations: {
        SET_DATA: (state, data) => {
            state.data = data
        }
    },
    gettters: {
        DATA: state => {
            return state.data
        }
    }
}