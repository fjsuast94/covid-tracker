import axios from '@axios'
const { token } = JSON.parse(localStorage.getItem('userData'))

axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

export default {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {
        fetchUsers(ctx, queryParams) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/api/users')
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        fetchUser(ctx, { id }) {
            return new Promise((resolve, reject) => {
                axios
                    .get(`/apps/user/users/${id}`)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
        addUser(ctx, userData) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/api/users', userData)
                    .then(response => resolve(response))
                    .catch(error => reject(error))
            })
        },
    },
}