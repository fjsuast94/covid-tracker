<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHospitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitals', function (Blueprint $table) {
            $table->id();
            
            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('image')->nullable();
            $table->longText('address');
            $table->string('description')->nullable();
            $table->string('phone');
            $table->integer('bedTotal');
            $table->integer('covidPatient');
            $table->integer('bedAvailable');
            $table->string('latitude');
            $table->string('longitude');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospitals');
    }
}