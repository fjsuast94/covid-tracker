<?php

namespace Database\Factories;

use App\Models\Hospital;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class HospitalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Hospital::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $bedTotal = $this->faker->unique(true)->numberBetween(1, 1000);
        $covidPatient = $this->faker->unique(true)->numberBetween(1, $bedTotal);
        $bedAvailable = $bedTotal - $covidPatient;

        return [
            'bedTotal' => $bedTotal,
            'covidPatient' => $covidPatient,
            'bedAvailable' => $bedAvailable,
            'description' => $this->faker->text(100),
        ];
    }
}