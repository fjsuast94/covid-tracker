<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456789'),
            'remember_token' => Str::random(10),
            'rol' => 'admin',
        ]);
        
        User::factory(9)->create();
    }
}
