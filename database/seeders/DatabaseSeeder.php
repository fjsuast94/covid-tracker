<?php

namespace Database\Seeders;

use App\Models\Hospital;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /* Storage::deleteDirectory('hospitals');
        Storage::deleteDirectory('users');

        Storage::makeDirectory('hospitals');
        Storage::makeDirectory('users'); */

        
        $this->call(UserSeeder::class);
        $this->call(HospitalSeeder::class);
        
    }
}