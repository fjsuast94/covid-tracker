<?php

namespace Database\Seeders;

use App\Models\Hospital;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;


class HospitalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hospitals = [
            [
                'name' => 'Hospital General Dr. Donato G. Alarcón', 
                'slug' => Str::slug('Hospital General Dr. Donato G. Alarcón'), 
                'address' => 'Av. Juan R. Escudero 158, Cd Renacimiento, 39715 Acapulco de Juárez, Gro.',
                'phone' => '744 469 9060',
                "latitude" => '16.899851300361206',
                "longitude" => '-99.82757893330107',
                'image' => 'images/hospital/Hospital-General-Dr-Donato-G-Alarcón.jpeg'
            ],
            [
                'name' => 'Hospital General De Acapulco', 
                'slug' => Str::slug('Hospital General De Acapulco'), 
                'address' => 'Carr. Fed. México - Acapulco Lt. 1 - 2, Los Mangos (El Quemado, 39910 Acapulco de Juárez, Gro.',
                'phone' => 'S/N',
                "latitude" => '16.93245679147161',
                "longitude" => '-99.82021959492215',
                'image' => 'images/hospital/hospital-general-acapulco.jpeg'
            ],
            [
                'name' => 'Hospital Naval De Acapulco', 
                'slug' => Str::slug('Hospital naval de acapulco'), 
                'address' => 'Acapulco - Aeropuerto Acapulco 1018, Icacos, 39860 Acapulco de Juárez, Gro.',
                'phone' => '744 484 7053',
                "latitude" => '16.840803438702658',
                "longitude" => '-99.84903391192393',
                'image' => 'images/hospital/hospital-naval-de-acapulco.jpeg'
            ],
            [
                'name' => 'Hospital General ISSSTE', 
                'slug' => Str::slug('Hospital General ISSSTE'), 
                'address' => 'Av. Adolfo Ruiz Cortines 124, CTM, 39601 Acapulco de Juárez, Gro.',
                'phone' => '744 435 0770',
                "latitude" => '16.875878886654863',
                "longitude" => '-99.88728516477539',
                'image' => 'images/hospital/Hospital-General-ISSSTE.jpeg'
            ],
            [
                'name' => 'IMSS Hospital General Regional Vicente Guerrero (HGR No. 1)', 
                'slug' => Str::slug('IMSS Hospital General Regional Vicente Guerrero (HGR No. 1)'), 
                'address' => 'Av. Adolfo Ruiz Cortinez #7, Progreso, 39610 Acapulco de Juárez, Gro.',
                'phone' => '744 445 5371',
                "latitude" => '16.876162474247803',
                "longitude" => '-99.8887084288255',
                'image' => 'images/hospital/IMSS-Hospital-General.png'
            ],
        ];

        foreach($hospitals as $hospital){
            Hospital::factory(1)->create($hospital);
        }
    }
}