<?php

namespace App\Http\Controllers;

use App\Models\Bitacora;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
        $this->guard = "api";
    }

    public function login(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $error = [
                "status" => false,
                "message" => $validator->errors()->first()
            ];
            return response()->json($error, 422);
        }


        try {
            if (!$token = JWTAuth::attempt($validator->validated())) {
                $error = [
                    "status" => false,
                    "message" => "Usuario o contraseña incorrecta",
                ];

                return response()->json($error, 422);
            }
        } catch (JWTException $e) {
            $error = [
                "status" => false,
                "message" => "Hubo un error con el sistema intente de nuevo"
            ];
            return response()->json($error, 500);
        }

        Bitacora::create([
            'user' => Auth::user()->name,
            'type' => 'Inicio de sesión',
            'icon' => 'LogInIcon',
            'description' => 'El usuario ' .Auth::user()->name. ' ha iniciado sesión.'     
        ]);

        $response = [
            'status' => true,
            'token' => $token,
            'user' => auth()->user()
        ];
        
        return response()->json($response);
        
    }

}