<?php

namespace App\Http\Controllers;

use App\Models\Bitacora;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Carbon::setLocale('es');
        $users = User::latest('id')->get()->map(function($item) {
            $item->ago = $item->updated_at->diffForHumans();
            $item->photo = Storage::url($item->photo);
            return $item;
        });

        Bitacora::create([
            'user' => Auth::user()->name,
            'icon' => 'EyeIcon',
            'type' => 'Listar usuarios',
            'description' => 'El usuario ' .Auth::user()->name. ' ha consultado la lista de usuarios.'     
        ]);

        return response()->json(
            [
                'status' => true, 
                'data' => $users,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = ['status' => false, 'data' => [], 'message' => ''];

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'name' => 'required',
            'rol' => 'required',
        ]);

        if ($validator->fails()) {
            $response['message'] = $validator->errors()->first();
            return response()->json($response, 422);
        }

        $request = array_merge($request->all(), ['password' => bcrypt('123456789')]);
        
        User::create($request);
        Bitacora::create([
            'user' => Auth::user()->name,
            'icon' => 'UserPlusIcon',
            'type' => 'Agregar usuario',
            'description' => 'El usuario ' .Auth::user()->name. ' ha registrado un nuevo usuario.'     
        ]);

        $response['status'] = true;
        $response['message'] = 'Se ha registrado un nuevo usuario correcto';

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        Bitacora::create([
            'user' => Auth::user()->name,
            'icon' => 'Trash2Icon',
            'type' => 'Eliminar usuario',
            'description' => 'El usuario ' .Auth::user()->name. ' ha eliminado un usuario de la lista'     
        ]);
        return response()->json(['status' => true, 'message' => 'Usuario eliminado correctamente']);
    }
}
