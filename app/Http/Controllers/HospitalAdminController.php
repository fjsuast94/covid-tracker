<?php

namespace App\Http\Controllers;

use App\Models\Bitacora;
use App\Models\Hospital;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class HospitalAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Carbon::setLocale('es');
        $hospitals = Hospital::get()->map(function($item) {
            $item->ago = $item->updated_at->diffForHumans();
            $item->image = $item->image ? Storage::url($item->image) : 'https://i1.wp.com/www.musicapopular.cult.cu/wp-content/uploads/2017/12/imagen-no-disponible.png';
            return $item;
        });

        Bitacora::create([
            'user' => Auth::user()->name,
            'icon' => 'EyeIcon',
            'type' => 'Listar hospitales',
            'description' => 'El usuario ' .Auth::user()->name. ' ha consultado la lista de hospitales.'     
        ]);

        return response()->json(
            [
                'status' => true, 
                'data' => $hospitals,
            ]
        );
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = ['status' => false, 'data' => [], 'message' => ''];

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'bedTotal' => 'required',
            'bedAvailable' => 'required',
            'covidPatient' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        if ($validator->fails()) {
            $response['message'] = $validator->errors()->first();
            return response()->json($response, 422);
        }
        $request = array_merge(
            $request->all(), 
            [
                'slug' => Str::slug($request->name)
            ]);
        

        Hospital::create($request);
        Bitacora::create([
            'user' => Auth::user()->name,
            'icon' => 'BookOpenIcon',
            'type' => 'Agregar hospital',
            'description' => 'El usuario ' .Auth::user()->name. ' ha registrado un nuevo hospital.'     
        ]);

        $response['status'] = true;
        $response['message'] = 'Se ha registrado un nuevo hospital correcto';

        return response()->json($response);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hospital = Hospital::find($id);
        Carbon::setLocale('es');
        if(empty($hospitals) ) {
            $hospital->image = $hospital->image ? Storage::url($hospital->image) : 'https://i1.wp.com/www.musicapopular.cult.cu/wp-content/uploads/2017/12/imagen-no-disponible.png';
            $hospital->ago = $hospital->updated_at->diffForHumans();
        }

        Bitacora::create([
            'user' => Auth::user()->name,
            'icon' => 'BookIcon',
            'type' => 'Consultar detalle',
            'description' => 'El usuario ' .Auth::user()->name. ' ha consultado el detalle del hsopital ' . $hospital->name .'.' 
        ]);

        return response()->json(
            [
                'status' => true, 
                'data' => $hospital,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function edit(Hospital $hospital)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hospital $hospital)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hospital $hospital)
    {
        //
    }
}