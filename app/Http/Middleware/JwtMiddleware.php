<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $error = [
            'status'    => false,
            'message'   => '',
        ];

        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                $error['message'] = 'Token is Invalid';
                return response()->json($error, 401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                $error['message'] = 'Token is Expired';
                return response()->json($error, 401);
            }else{
                $error['message'] = 'Authorization Token not found';
                return response()->json($error, 401);
            }
        }
        return $next($request);

    }
}
