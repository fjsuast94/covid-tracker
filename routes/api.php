<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BitacoraController;
use App\Http\Controllers\HospitalAdminController;
use App\Http\Controllers\HospitalController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('api')->group(function () {
    Route::resource('hospitals', HospitalController::class);
    Route::post('auth/login', [AuthController::class, 'login'])->name('auth.login');
});

Route::group(['middleware' => ['jwt.verify']], function () {
    Route::resource('/users', UserController::class);
    Route::resource('/bitacoras', BitacoraController::class);
    Route::resource('/hospitals-admin', HospitalAdminController::class);
});